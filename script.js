const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const arr1 = ["1", "2", "3", "sea", "user", 23];
const arr2 = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

function list(arr, parent = document.body) {
    let listDOM = document.createElement('ul');
    parent.append(listDOM);
    arr.forEach((elem) => {
        if (Array.isArray(elem)) {
            let liDOM = document.createElement('li');
            listDOM.append(liDOM);
            list(elem, liDOM);
        } else {
            let liDOM = document.createElement('li');
            liDOM.textContent = elem;
            listDOM.append(liDOM);
        }
    });
}

list(arr2, document.getElementById('header'));

